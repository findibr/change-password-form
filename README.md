### Dependencies
Ionic
[Angular formly](https://github.com/formly-js/angular-formly)
[Angular formly ionic](https://github.com/formly-js/angular-formly-templates-ionic)
```html
<link rel="stylesheet" href="lib/intlpnIonic/css/intlpn.css">

<script src="lib/ionic/js/ionic.bundle.js"></script>
<script src="lib/api-check/dist/api-check.min.js"></script>
<script src="lib/angular-formly/dist/formly.min.js"></script>
<script src="lib/angular-formly-templates-ionic/dist/angular-formly-templates-ionic.js"></script>
<script src="lib/change-password-form/change-password-form.js"></script>
```

### Insert module
```javascript
angular.module('myModule', ['findi.changePasswordForm'])
```

### Use
```html
<change-password-form ng-model="myModel" options="inputOptions"></change-password-form>
```
### inputOptions
```javascript
{
	configFields: {
		oldPassword: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
			minlength: 'INT' // Change minlength input
		},
		newPassword: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model,
			minlength: 'INT' // Change minlength input
		},
		confirmPassword: { // optional
			hide: 'BOOLEAN' // Remove input to form
			label: 'STRING' // Change label input
			placeholder: 'STRING' // Change placeholder input
			key: 'STRING' // Change variable from model
			minlength: 'INT' // Change minlength input
		}
	},
	submit: function(data, form) { // required
		// data: myModel
		// form: form
	},
	submitButtonText: 'STRING' // Text button submit
}
```
