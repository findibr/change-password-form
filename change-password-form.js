(function() {
	'use strict'
	var DEFAULT_KEY_NEW_PASSWORD = 'newPassword'

	angular.module('findi.changePasswordForm', ['ionic', 'formlyIonic'])
		.component('changePasswordForm', {
			template: '<form name="{{$ctrl.opts.formName}}" novalidate="novalidate" ng-submit="$ctrl.opts.submit($ctrl.ngModel)">' +
				'<formly-form model="$ctrl.ngModel" fields="$ctrl.opts.fields"></formly-form>' +
				'<input class="findi-btn" type="submit" value="{{$ctrl.opts.submitButtonText}}"/>' +
				'</form>',
			controller: controller,
			controllerAs: '$ctrl',
			bindings: {
				ngModel: '=',
				options: '='
			}
		})

	function controller($scope, formlyConfig) {
		var vm = this
		var optionsParams = angular.copy(vm.options) || {}
		vm.opts = _buildOpts(vm, optionsParams)
	}

	var _buildOpts = function(vm, optionsParams) {
		var opts = {}
		opts.formName = "$ctrl.changePasswordForm"
		opts.configFields = optionsParams.configFields || {}
		opts.submitButtonText = optionsParams.submitButtonText || 'Salvar'
		opts.fields = _getFields(opts.configFields)
		opts.submit = function(data) {
			optionsParams.submit(data, vm.changePasswordForm)
		}

		return opts
	}

	var _getFields = function(configFields) {
		var array = []
		var configOldPasswordField = configFields.oldPassword || {}
		var configNewPasswordField = configFields.newPassword || {}
		var configConfirmPasswordField = configFields.confirmPassword || {}
		configConfirmPasswordField.newPasswordKey = configNewPasswordField.key ?
			configNewPasswordField.key : DEFAULT_KEY_NEW_PASSWORD

		if (!configOldPasswordField.hide) array.push(_getOldPasswordField(
			configOldPasswordField))
		if (!configNewPasswordField.hide) array.push(_getNewPasswordField(
			configNewPasswordField))
		if (!configConfirmPasswordField.hide) array.push(_getConfirmPasswordField(
			configConfirmPasswordField))

		return array
	}

	var _getOldPasswordField = function(configField) {
		return {
			key: configField.key ? configField.key : "oldPassword",
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "password",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'SENHA ATUAL',
				minlength: configField.minlength ? configField.minlength : 8
			}
		}
	}

	var _getNewPasswordField = function(configField) {
		return {
			key: configField.key ? configField.key : DEFAULT_KEY_NEW_PASSWORD,
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "password",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'NOVA SENHA',
				minlength: configField.minlength ? configField.minlength : 8
			}
		}
	}

	var _getConfirmPasswordField = function(configField) {
		return {
			key: configField.key ? configField.key : 'confirmPassword',
			type: "stacked-input",
			templateOptions: {
				required: true,
				type: "password",
				placeholder: configField.placeholder ? configField.placeholder : "",
				label: configField.label ? configField.label : 'CONFIRMAR SENHA',
				minlength: configField.minlength ? configField.minlength : 8
			},
			validators: {
				confirmPassword: {
					expression: function($viewValue, $modelValue, scope) {
						var newPasswordKey = configField.newPasswordKey
						return ($viewValue == scope.model[newPasswordKey]);
					}
				}
			}
		}
	}
})()
